package com.biros.kafkaadmin.spring.view;

import com.biros.kafkaadmin.spring.model.KafkaService;
import com.biros.kafkaadmin.spring.service.KafkaSchemaRegistryService;
import com.biros.kafkaadmin.spring.util.HostUtil;
import com.biros.kafkaadmin.spring.view.component.MainNavigation;
import com.biros.kafkaadmin.spring.view.component.SchemaListWidget;
import com.biros.kafkaadmin.spring.view.component.SchemaVersionsTabsWidget;
import com.biros.kafkaadmin.spring.view.component.ScrollLayout;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.PreserveOnRefresh;
import com.vaadin.flow.router.Route;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Push
@Route("schemas")
@PreserveOnRefresh
@CssImport("./styles/style.css")
public class SchemaView extends AppLayout implements AfterNavigationObserver {
    private final Logger LOG = LoggerFactory.getLogger(SchemaView.class);

    private KafkaSchemaRegistryService kafkaSchemaService;

    private HorizontalLayout splitLayout;

    private VerticalLayout leftSide;
    private VerticalLayout rightSide;
    private ScrollLayout schemaList;
    private Div schemaDetails;
    private Select<KafkaService> hostSelect;

    private List<String> lastSchemaResult;
    private List<KafkaService> connectorServiceUrls = HostUtil.getSchemaRegistryHosts();

    @Autowired
    public SchemaView(KafkaSchemaRegistryService kafkaSchemaRegistryService) {

        addToNavbar(new MainNavigation(3));

        this.kafkaSchemaService = kafkaSchemaRegistryService;
        this.setId("SchemaView");

        splitLayout = new HorizontalLayout();
        splitLayout.setSizeFull();
        setContent(splitLayout);

        leftSide = new VerticalLayout();
        splitLayout.add(leftSide);
        rightSide = new VerticalLayout();
        splitLayout.add(rightSide);

        hostSelect = new Select<>();
        hostSelect.setItemLabelGenerator(KafkaService::getLabel);
        hostSelect.setItems(connectorServiceUrls);
        hostSelect.setEmptySelectionAllowed(false);
        hostSelect.setValue(connectorServiceUrls.get(0));
        hostSelect.addValueChangeListener((event) -> refreshSchemas());
        leftSide.add(hostSelect);

        schemaList = new ScrollLayout();
        leftSide.add(schemaList);

        Accordion accordion = new Accordion();
        accordion.add("First", new Label("first label"));
        accordion.add("Second", new Label("second label"));
        accordion.close();
        rightSide.add(accordion);


        schemaDetails = new Div();
        schemaDetails.getElement().setAttribute("style", "white-space: pre-wrap;");
        rightSide.add(schemaDetails);

        lastSchemaResult = new ArrayList<>();
    }

    private void refreshSchemas() {
        LOG.debug("refresh schemas");
        LOG.debug("element id: {}", getId());
        rightSide.removeAll();
        KafkaService service = hostSelect.getValue();
        List<String> schemaResult = kafkaSchemaService.getSubjects(service);
        if (schemaResult.size() != lastSchemaResult.size() || !CollectionUtils.isEqualCollection(lastSchemaResult, schemaResult)) {
            //refresh ui
            schemaList.removeAll();
            schemaResult.stream().forEach(schema -> schemaList.add(
                    new SchemaListWidget(schema, kafkaSchemaService, hostSelect,
                            schemaVersions -> renderSchemaDetails(schema, schemaVersions),
                            () -> refreshSchemas())));
            lastSchemaResult = schemaResult;
        }
    }

    private void renderSchemaDetails(String schemaName, List<String> schemaVersions) {
        rightSide.removeAll();
        rightSide.add(new SchemaVersionsTabsWidget(schemaName, schemaVersions, kafkaSchemaService, hostSelect.getValue()));
    }

    @Override
    public void afterNavigation(AfterNavigationEvent afterNavigationEvent) {
        refreshSchemas();
    }
}

