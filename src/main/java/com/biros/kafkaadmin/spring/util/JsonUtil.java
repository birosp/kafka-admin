package com.biros.kafkaadmin.spring.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class JsonUtil {
    private static final Logger LOG = LoggerFactory.getLogger(JsonUtil.class);

    public static String prettyfyJsonString(String input) {
        String result = "";
        ObjectMapper mapper = new ObjectMapper();
        Object jsonObject = null;
        try {
            jsonObject = mapper.readValue(input, Object.class);
            result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject);
        } catch (IOException ioe) {
            LOG.error("Error while parsing json response", ioe);
        }
        return result;
    }

    public static String removeBackSlash(String input) {
        return input.replace("\\", "");
    }
}
