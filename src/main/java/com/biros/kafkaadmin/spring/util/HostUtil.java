package com.biros.kafkaadmin.spring.util;

import com.biros.kafkaadmin.spring.model.KafkaService;

import java.util.List;

public class HostUtil {
    public static List<KafkaService> getConnectorHosts() {
        return List.of(
                new KafkaService("localhost", "http://localhost:8083", "", "")
        );
    }

    public static List<KafkaService> getSchemaRegistryHosts() {
        return List.of(
                new KafkaService("localhost", "http://localhost:8081", "", "")
        );
    }

    public static List<KafkaService> getTopicHosts() {
        return List.of(
                new KafkaService("localhost", "http://localhost:8082", "", "")
        );
    }
}
