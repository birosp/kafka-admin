package com.biros.kafkaadmin.spring.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class KafkaTopicService {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaTopicService.class);

    private static String topicServiceUrl = "http://localhost:8082";

    private final List<String> systemTopicPrefixes = new ArrayList<>(Arrays.asList("__confluent", "_confluent", "connect-", "_schemas"));

    private boolean isSystemTopic(String topicName) {
        return systemTopicPrefixes.stream().anyMatch(topicName::startsWith);
    }

    public List<String> getTopics(boolean showSystemTopics) {
        RestTemplate restTemplate = new RestTemplate();
        List<String> topicList = new ArrayList<>();
        try {
            topicList = restTemplate.getForObject(topicServiceUrl + "/topics", List.class);

        } catch (Exception e) {
            LOG.error("Error while getting the topics: ", e);
        }
        return topicList != null ? topicList.stream().filter(topic -> showSystemTopics || !isSystemTopic(topic)).collect(Collectors.toList()) : Collections.emptyList();
    }

    public String getTopicInfo(String topicName) {
        RestTemplate restTemplate = new RestTemplate();
        String topicDetails = "";
        try {
            topicDetails = restTemplate.getForObject(topicServiceUrl + "/topics/" + topicName, String.class);

        } catch (Exception e) {
            LOG.error("Error while getting the topics: ", e);
        }
        return topicDetails;
    }
}
