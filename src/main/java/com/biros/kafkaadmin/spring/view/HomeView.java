package com.biros.kafkaadmin.spring.view;

import com.biros.kafkaadmin.spring.view.component.MainNavigation;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;

@Route("")
@PageTitle("Kafka Admin")
@CssImport("./styles/style.css")
@Viewport("width=device-width, minimum-scale=1, initial-scale=1, user-scalable=yes, viewport-fit=cover")
@PWA(name = "Project Base for Vaadin Flow with Spring", shortName = "Project Base")
public class HomeView extends AppLayout {

    public HomeView() {
        addToNavbar(new MainNavigation(0));
    }
}
