package com.biros.kafkaadmin.spring.view;

import com.biros.kafkaadmin.spring.model.KafkaService;
import com.biros.kafkaadmin.spring.service.KafkaTopicService;
import com.biros.kafkaadmin.spring.util.HostUtil;
import com.biros.kafkaadmin.spring.util.JsonUtil;
import com.biros.kafkaadmin.spring.view.component.MainNavigation;
import com.biros.kafkaadmin.spring.view.component.ScrollLayout;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.PreserveOnRefresh;
import com.vaadin.flow.router.Route;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Push
@Route("topics")
@PreserveOnRefresh
@CssImport("./styles/style.css")
public class TopicView extends AppLayout implements AfterNavigationObserver {
    private final Logger LOG = LoggerFactory.getLogger(TopicView.class);

    private KafkaTopicService kafkaTopicService;

    private HorizontalLayout splitLayout;

    private VerticalLayout leftSide;
    private ScrollLayout rightSide;
    private Checkbox showSystemTopicsCheckbox;
    private Select<KafkaService> hostSelect;
    private ScrollLayout topicList;
    private Div topicDetails;

    private List<String> lastTopicResult;
    private List<KafkaService> kafkaKafkaServiceUrls = HostUtil.getTopicHosts();

    @Autowired
    public TopicView(KafkaTopicService kafkaTopicService) {

        addToNavbar(new MainNavigation(1));

        this.kafkaTopicService = kafkaTopicService;
        this.setId("TopicView");

        splitLayout = new HorizontalLayout();
        splitLayout.setSizeFull();
        setContent(splitLayout);

        leftSide = new VerticalLayout();
        leftSide.setSizeFull();
        splitLayout.add(leftSide);

        showSystemTopicsCheckbox = new Checkbox("Show system topics", false);
        showSystemTopicsCheckbox.addClickListener((e) -> {
            refreshTopics();
        });
        leftSide.add(showSystemTopicsCheckbox);
        hostSelect = new Select<>();
        hostSelect.setItemLabelGenerator(KafkaService::getLabel);
        hostSelect.setItems(kafkaKafkaServiceUrls);
        hostSelect.setEmptySelectionAllowed(false);
        hostSelect.setValue(kafkaKafkaServiceUrls.get(0));
        hostSelect.addValueChangeListener((event) -> refreshTopics());
        leftSide.add(hostSelect);

        rightSide = new ScrollLayout();
        rightSide.setSizeFull();
        splitLayout.add(rightSide);


        topicList = new ScrollLayout();
        leftSide.add(topicList);

        topicDetails = new Div();
        topicDetails.getElement().setAttribute("style", "white-space: pre-wrap;");
        rightSide.add(topicDetails);

        lastTopicResult = new ArrayList<>();
    }

    private void refreshTopics() {
        LOG.debug("refresh topics");
        LOG.debug("element id: {}", getId());
        LOG.debug("Show system topics: {}", showSystemTopicsCheckbox.getValue().toString());
        topicDetails.setText("");
        List<String> topicResult = kafkaTopicService.getTopics(showSystemTopicsCheckbox.getValue());
        if (topicResult.size() != lastTopicResult.size() || !CollectionUtils.isEqualCollection(lastTopicResult, topicResult)) {
            //refresh ui
            topicList.removeAll();
            topicResult.stream().forEach(topic -> topicList.add(new Button(topic, e -> {
                String topicName = e.getSource().getText();
                String topicInfo = kafkaTopicService.getTopicInfo(topicName);
                String prettyJson = JsonUtil.prettyfyJsonString(topicInfo);
                topicDetails.setText(prettyJson);
            })));
            lastTopicResult = topicResult;
        }
    }

    @Override
    public void afterNavigation(AfterNavigationEvent afterNavigationEvent) {
        refreshTopics();
    }
}
