package com.biros.kafkaadmin.spring.service;

import com.biros.kafkaadmin.spring.model.KafkaService;
import com.biros.kafkaadmin.spring.util.RestUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

//https://docs.confluent.io/current/connect/references/restapi.html

@Service
public class KafkaConnectorService {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaConnectorService.class);

    public List<String> getConnectors(KafkaService connectorService) {
        RestTemplate restTemplate = new RestTemplate();
        List<Object> connectorList = new ArrayList<>();
        HttpHeaders headers = RestUtil.createAuthHeader(connectorService.getUserName(), connectorService.getPassword());
        try {
            ResponseEntity<List> response = restTemplate.exchange(connectorService.getUrl() + "/connectors", HttpMethod.GET, new HttpEntity<>(headers), List.class);
            connectorList = response.getBody();

        } catch (Exception e) {
            LOG.error("Error while getting the connectors: ", e);
        }
        return Optional.of(connectorList).orElse(List.of()).stream().map(topic -> topic.toString()).collect(Collectors.toList());
    }

    public String getConnectorInfo(KafkaService connectorService, String connectorName) {
        RestTemplate restTemplate = new RestTemplate();
        String connectorDetails = "";
        HttpHeaders headers = RestUtil.createAuthHeader(connectorService.getUserName(), connectorService.getPassword());
        try {
            ResponseEntity<String> response = restTemplate.exchange(connectorService.getUrl() + "/connectors/" + connectorName, HttpMethod.GET, new HttpEntity<>(headers), String.class);
            connectorDetails = response.getBody();

        } catch (Exception e) {
            LOG.error("Error while getting the connectors: ", e);
        }
        return connectorDetails;
    }

    public String getConnectorConfig(KafkaService connectorService, String connectorName) {
        RestTemplate restTemplate = new RestTemplate();
        String connectorConfig = "";
        HttpHeaders headers = RestUtil.createAuthHeader(connectorService.getUserName(), connectorService.getPassword());

        try {
            ResponseEntity<String> response = restTemplate.exchange(connectorService.getUrl() + "/connectors/" + connectorName + "/config", HttpMethod.GET, new HttpEntity<>(headers), String.class);
            connectorConfig = response.getBody();
        } catch (Exception e) {
            LOG.error("Error while getting the connectors: ", e);
        }
        return connectorConfig;
    }

    public String getConnectorStatus(KafkaService connectorService, String connectorName) {
        RestTemplate restTemplate = new RestTemplate();
        String connectorStatus = "";
        HttpHeaders headers = RestUtil.createAuthHeader(connectorService.getUserName(), connectorService.getPassword());
        try {
            ResponseEntity<String> response = restTemplate.exchange(connectorService.getUrl() + "/connectors/" + connectorName + "/status", HttpMethod.GET, new HttpEntity<>(headers), String.class);
            connectorStatus = response.getBody();
        } catch (Exception e) {
            LOG.error("Error while getting the connectors: ", e);
        }
        return connectorStatus;
    }

    public String getConnectorTasks(KafkaService connectorService, String connectorName) {
        RestTemplate restTemplate = new RestTemplate();
        String connectorTasks = "";
        HttpHeaders headers = RestUtil.createAuthHeader(connectorService.getUserName(), connectorService.getPassword());
        try {
            ResponseEntity<String> response = restTemplate.exchange(connectorService.getUrl() + "/connectors/" + connectorName + "/tasks", HttpMethod.GET, new HttpEntity<>(headers), String.class);
            connectorTasks = response.getBody();

        } catch (Exception e) {
            LOG.error("Error while getting the connectors: ", e);
        }
        return connectorTasks;
    }

    /**
     * Restart a connector
     *
     * @param connectorName name of the connector
     * @return true if the restart was successful
     */
    public boolean restartConnector(String connectorServiceUrl, String connectorName) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setMessageConverters(Collections.singletonList(new MappingJackson2HttpMessageConverter()));
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node = mapper.createObjectNode();
        HttpEntity<Object> entity = new HttpEntity<>(node.toString(), headers);
        String url = connectorServiceUrl + "/connectors/" + connectorName + "/restart";
        ResponseEntity<Object> result = restTemplate.exchange(url, HttpMethod.POST, entity, Object.class);
        return HttpStatus.NO_CONTENT.equals(result.getStatusCode());
    }

    public void pauseConnector() {

    }

    public void resumeConnector() {

    }
}
