package com.biros.kafkaadmin.spring.model;

import org.apache.commons.lang3.StringUtils;

public class KafkaService {
    private String name;
    private String url;
    private String userName;
    private String password;

    public KafkaService(String name, String url, String userName, String password) {
        this.name = name;
        this.url = url;
        this.userName = userName;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLabel() {
        if (StringUtils.isNotBlank(name)) {
            return name;
        } else {
            return url;
        }
    }
}
