package com.biros.kafkaadmin.spring.view.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class ScrollLayout extends VerticalLayout {

    private VerticalLayout content;

    public ScrollLayout() {
        super();
        preparePanel();
    }

    public ScrollLayout(Component... children) {
        super();
        preparePanel();
        this.add(children);
    }

    private void preparePanel() {
        getStyle().set("overflow-y", "auto");
        getStyle().set("flex", "1 1 auto");
        content = new VerticalLayout();
        super.add(content);
//        getStyle().set("background-color","rgb(230,230,230");
    }

    public VerticalLayout getContent() {
        return content;
    }

    @Override
    public void add(Component... components) {
        content.add(components);
    }

    @Override
    public void remove(Component... components) {
        content.remove(components);
    }

    @Override
    public void removeAll() {
        content.removeAll();
    }

    @Override
    public void addComponentAsFirst(Component component) {
        content.addComponentAtIndex(0, component);
    }
}
