package com.biros.kafkaadmin.spring.view.component;

import com.biros.kafkaadmin.spring.model.KafkaService;
import com.biros.kafkaadmin.spring.service.KafkaConnectorService;
import com.biros.kafkaadmin.spring.util.JsonUtil;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.select.Select;

public class ConnectorListWidget extends HorizontalLayout {
    public ConnectorListWidget(String connectorName, KafkaConnectorService kafkaConnectorService, Select<KafkaService> hostSelect, Div target) {
        setWidthFull();
        Button detailsButton = new Button(connectorName, e -> {
            e.getSource().getParent().get().getParent().get().getChildren().forEach(component -> ((ConnectorListWidget) component).getStyle().set("background-color", "unset"));
            this.getStyle().set("background-color", "rgb(230, 240, 255)");

            KafkaService service = hostSelect.getValue();

            String connectorConfig = kafkaConnectorService.getConnectorConfig(service, connectorName);
            String prettyConnectorConfig = JsonUtil.prettyfyJsonString(connectorConfig);

            String connectorStatus = kafkaConnectorService.getConnectorStatus(service, connectorName);
            String prettyConnectorStatus = JsonUtil.prettyfyJsonString(connectorStatus);

//            String connectorTasks = kafkaConnectorService.getConnectorTasks(service, connectorName);
//            String prettyConnectorTasks = JsonUtil.prettyfyJsonString(connectorTasks);

            target.setText("Config\n" + prettyConnectorConfig + "\n\nStatus\n" + prettyConnectorStatus);
        });
        detailsButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        add(detailsButton);

        if (hostSelect.getValue().getUrl().contains("localhost")) {
            Button restartButton = new Button("restart", e -> {
                ConfirmDialog dialog = new ConfirmDialog("Confirm restart",
                        "Are you sure you want to restart the " + connectorName + " connector?", "Restart", (re) -> {
                    boolean result = kafkaConnectorService.restartConnector(hostSelect.getValue().getUrl(), connectorName);
                    if (result) {
                        Notification notification = new Notification("Connector " + connectorName + " restarted.", 5000);
                        notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
                        notification.open();
                    }
                },
                        "Cancel", (ce) -> {});
                dialog.setConfirmButtonTheme("error primary");
                dialog.open();
            });
            add(restartButton);
        }
    }
}
