package com.biros.kafkaadmin.spring.service;

import com.biros.kafkaadmin.spring.model.KafkaService;
import com.biros.kafkaadmin.spring.util.RestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

//https://docs.confluent.io/current/schema-registry/docs/api.html

@Service
public class KafkaSchemaRegistryService {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaSchemaRegistryService.class);

    public List<String> getSubjects(KafkaService service) {
        RestTemplate restTemplate = new RestTemplate();
        List<String> subjects = new ArrayList<>();
        HttpHeaders headers = RestUtil.createAuthHeader(service.getUserName(), service.getPassword());
        try {
            ResponseEntity<List> response = restTemplate.exchange(service.getUrl() + "/subjects", HttpMethod.GET, new HttpEntity<>(headers), List.class);
            subjects = response.getBody();

        } catch (Exception e) {
            LOG.error("Error while getting the topics: ", e);
        }
        return subjects;
    }

    public List<Object> getSubjectVersions(KafkaService service, String subjectName) {
        RestTemplate restTemplate = new RestTemplate();
        List<Object> versions = new ArrayList<>();
        try {
            versions = restTemplate.getForObject(service.getUrl() + "/subjects/" + subjectName + "/versions", List.class);

        } catch (Exception e) {
            LOG.error("Error while getting the schemas: ", e);
        }
        return versions;
    }

    public String getSubjectVersion(KafkaService service, String subjectName, String version) {
        RestTemplate restTemplate = new RestTemplate();
        String result = "";
        try {
            result = restTemplate.getForObject(service.getUrl() + "/subjects/" + subjectName + "/versions/" + version + "/schema", String.class);
        } catch (Exception e) {
            LOG.error("Error while getting the schemas: ", e);
        }
        return result;
    }

    public void deleteSubject(KafkaService service, String subjectName) {
        if (!service.getUrl().contains("localhost")) {
            return;
        }
        RestTemplate restTemplate = new RestTemplate();
        String url = service.getUrl();
        if (url.contains("localhost")) {
            try {
                restTemplate.delete(service.getUrl() + "/subjects/" + subjectName);

            } catch (Exception e) {
                LOG.error("Error while getting the schemas: ", e);
            }
        }
    }

    public void deleteSubjectVersion(KafkaService service, String subjectName, String version) {
        if (!service.getUrl().contains("localhost")) {
            return;
        }
        RestTemplate restTemplate = new RestTemplate();
        String url = service.getUrl();
        if (url.contains("localhost")) {
            try {
                restTemplate.delete(service.getUrl() + "/subjects/" + subjectName + "/versions/" + version);
            } catch (Exception e) {
                LOG.error("Error while getting the schemas: ", e);
            }
        }
    }
}
