package com.biros.kafkaadmin.spring.view.component;

import com.biros.kafkaadmin.spring.view.ConnectorView;
import com.biros.kafkaadmin.spring.view.HomeView;
import com.biros.kafkaadmin.spring.view.SchemaView;
import com.biros.kafkaadmin.spring.view.TopicView;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.RouterLink;

public class MainNavigation extends Tabs {
    public MainNavigation(int selectedMenu) {
        Tab homeTab = new Tab(new RouterLink("Home", HomeView.class));
        Tab topicsTab = new Tab(new RouterLink("Topics", TopicView.class));
        Tab connectorsTab = new Tab(new RouterLink("Connectors", ConnectorView.class));
        Tab schemasTab = new Tab(new RouterLink("Schemas", SchemaView.class));

        add(homeTab, topicsTab, connectorsTab, schemasTab);
        setOrientation(Tabs.Orientation.HORIZONTAL);
        setSelectedIndex(selectedMenu);
    }
}
