package com.biros.kafkaadmin.spring.view.component;

import com.biros.kafkaadmin.spring.model.KafkaService;
import com.biros.kafkaadmin.spring.service.KafkaSchemaRegistryService;
import com.biros.kafkaadmin.spring.util.JsonUtil;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;

import java.util.List;

public class SchemaVersionsTabsWidget extends VerticalLayout {

    String schemaName;
    KafkaService service;
    KafkaSchemaRegistryService schemaRegistryService;

    Tabs tabs;
    Div schemaDetails;

    public SchemaVersionsTabsWidget(String schemaName, List<String> versions, KafkaSchemaRegistryService schemaRegistryService, KafkaService service) {
        setSizeFull();
        this.schemaName = schemaName;
        this.schemaRegistryService = schemaRegistryService;
        this.service = service;

        tabs = new Tabs();
        tabs.setFlexGrowForEnclosedTabs(1);
        versions.forEach(version -> tabs.add(new Tab(version)));
        tabs.setSelectedIndex(versions.size() - 1);

        ScrollLayout scrollLayout = new ScrollLayout();
        schemaDetails = new Div();
        schemaDetails.getElement().setAttribute("style", "white-space: pre-wrap;");
        scrollLayout.add(schemaDetails);

        tabs.addSelectedChangeListener(event -> getSelectedDetails());
        add(tabs);
        add(scrollLayout);

        getSelectedDetails();
    }

    public void getSelectedDetails() {
        String version = tabs.getSelectedTab().getLabel();
        String result = schemaRegistryService.getSubjectVersion(service, schemaName, version);
        schemaDetails.setText(JsonUtil.prettyfyJsonString(result));
    }
}
