package com.biros.kafkaadmin.spring.view.component;

import com.biros.kafkaadmin.spring.model.KafkaService;
import com.biros.kafkaadmin.spring.service.KafkaSchemaRegistryService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.select.Select;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class SchemaListWidget extends HorizontalLayout {

    public SchemaListWidget(String schemaName, KafkaSchemaRegistryService schemaRegistryService, Select<KafkaService> hostSelect, Consumer<List<String>> detailsCallback, Runnable deleteCallback) {

        setWidthFull();

        Button detailsButton = new Button(schemaName, e -> {

            e.getSource().getParent().get().getParent().get().getChildren().forEach(component -> ((SchemaListWidget) component).getStyle().set("background-color", "unset"));
            this.getStyle().set("background-color", "rgb(230, 240, 255)");

            KafkaService service = hostSelect.getValue();
            List<Object> schemaVersions = schemaRegistryService.getSubjectVersions(service, schemaName);
            detailsCallback.accept(schemaVersions.stream().map(Object::toString).collect(Collectors.toList()));
        });
        detailsButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        add(detailsButton);

        if (hostSelect.getValue().getUrl().contains("localhost")) {
            Button deleteButton = new Button("delete", e -> {
                ConfirmDialog dialog = new ConfirmDialog("Confirm delete",
                        "Are you sure you want to delete the " + schemaName + " schema?", "Delete", (de) -> {
                    schemaRegistryService.deleteSubject(hostSelect.getValue(), schemaName);
                    deleteCallback.run();
                },
                        "Cancel", (ce) -> {});
                dialog.setConfirmButtonTheme("error primary");
                dialog.open();
            });
            add(deleteButton);
        }
    }
}
