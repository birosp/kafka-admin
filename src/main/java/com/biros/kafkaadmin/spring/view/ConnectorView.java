package com.biros.kafkaadmin.spring.view;

import com.biros.kafkaadmin.spring.model.KafkaService;
import com.biros.kafkaadmin.spring.service.KafkaConnectorService;
import com.biros.kafkaadmin.spring.util.HostUtil;
import com.biros.kafkaadmin.spring.view.component.ConnectorListWidget;
import com.biros.kafkaadmin.spring.view.component.MainNavigation;
import com.biros.kafkaadmin.spring.view.component.ScrollLayout;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.PreserveOnRefresh;
import com.vaadin.flow.router.Route;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Push
@Route("connectors")
@PreserveOnRefresh
@CssImport("./styles/style.css")
public class ConnectorView extends AppLayout implements AfterNavigationObserver {
    private final Logger LOG = LoggerFactory.getLogger(ConnectorView.class);

    private KafkaConnectorService kafkaConnectorService;
    private HorizontalLayout splitLayout;
    private VerticalLayout leftSide;
    private VerticalLayout rightSide;
    private VerticalLayout connectorList;
    private Select<KafkaService> hostSelect;
    private Div connectorDetails;

    private List<String> lastConnectorResult;
    private List<KafkaService> connectorServiceUrls = HostUtil.getConnectorHosts();

    @Autowired
    public ConnectorView(KafkaConnectorService kafkaConnectorService) {

        addToNavbar(new MainNavigation(2));

        this.kafkaConnectorService = kafkaConnectorService;
        setId("ConnectorView");

        splitLayout = new HorizontalLayout();
        splitLayout.setSizeFull();
        setContent(splitLayout);

        leftSide = new VerticalLayout();
        splitLayout.add(leftSide);
        rightSide = new ScrollLayout();
        splitLayout.add(rightSide);

        hostSelect = new Select<>();
        hostSelect.setItemLabelGenerator(KafkaService::getLabel);
        hostSelect.setItems(connectorServiceUrls);
        hostSelect.setEmptySelectionAllowed(false);
        hostSelect.setValue(connectorServiceUrls.get(0));
        hostSelect.addValueChangeListener((event) -> refreshConnectors());
        leftSide.add(hostSelect);

        connectorList = new ScrollLayout();
        leftSide.add(connectorList);

        connectorDetails = new Div();
        connectorDetails.getElement().setAttribute("style", "white-space: pre-wrap;");
        rightSide.add(connectorDetails);

        lastConnectorResult = new ArrayList<>();
    }

    private void refreshConnectors() {
        LOG.debug("refresh connectors");
        LOG.debug("element id: {}", getId());
        connectorDetails.setText("");
        List<String> connectorResult = kafkaConnectorService.getConnectors(hostSelect.getValue());
        if (connectorResult.size() != lastConnectorResult.size() || !CollectionUtils.isEqualCollection(lastConnectorResult, connectorResult)) {
            //refresh ui
            connectorList.removeAll();
            connectorResult.stream().forEach(connectorName -> connectorList.add(
                    new ConnectorListWidget(connectorName, kafkaConnectorService, hostSelect, connectorDetails)));
            lastConnectorResult = connectorResult;
        }

    }

    @Override
    public void afterNavigation(AfterNavigationEvent afterNavigationEvent) {
        refreshConnectors();
    }
}